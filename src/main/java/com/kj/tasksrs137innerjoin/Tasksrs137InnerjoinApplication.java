package com.kj.tasksrs137innerjoin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tasksrs137InnerjoinApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tasksrs137InnerjoinApplication.class, args);
	}

}

package com.kj.tasksrs137innerjoin.dao.mapper;

        import java.util.List;

        import com.kj.tasksrs137innerjoin.model.AnnualPayment;
        import com.kj.tasksrs137innerjoin.model.Students;
        import com.kj.tasksrs137innerjoin.model.StudentsPaymentByID;
        import org.apache.ibatis.annotations.*;



public interface StudentsMapper {


    @ResultMap("students")
    @Select("SELECT * FROM students")
    List<Students> getAllData();


    @Insert("Insert INTO students(students_name, students_contact) values (#{studentsName}, #{studentsContact})")
    @SelectKey(statement = "SELECT LASTVAL()", keyProperty = "studentsID", keyColumn = "students_id", before = false, resultType = Integer.class)
    long insertDataIntoStudents(Students students);

    @Insert("Insert INTO annual_payment(students_id, transferred_amount,year_of_payment) values (#{studentsID}, #{transferredAmount}, #{yearOfPayment})")
    @SelectKey(statement = "SELECT LASTVAL()", keyProperty = "paymentID", keyColumn = "payment_id", before = false, resultType = Integer.class)
    long insertDataIntoAnnualPayment(AnnualPayment annualPayment);

    @Delete(" DELETE FROM students WHERE students_id=#{id}")
    void deleteDataByID(Integer id);

    @Delete(" DELETE FROM students WHERE students_name=#{studentsName}")
    void deleteDataByName(String studentsName);



    @Results(id = "students", value = {
            @Result(property = "studentsID", column = "students_id"),
            @Result(property = "studentsName", column = "students_name"),
            @Result(property = "studentsContact", column = "students_contact"),
    })
    @Select("SELECT students_id, students_name, students_contact FROM students WHERE students_id=#{id}")
    Students getStudentsByID(Integer id);

    @Results(id = "AnnualPayment", value = {
            @Result(property = "studentsID", column = "students_id"),
            @Result(property = "transferredAmount", column = "transferred_amount"),
            @Result(property = "yearOfPayment", column = "year_of_payment"),
    })
    @Select("SELECT students_id, transferred_amount, year_of_payment FROM annual_payment WHERE students_id=#{id}" )
    List<AnnualPayment> getAnnualPaymentByID(Integer id);



    @Results(id = "Students payment by ID", value = {
            @Result(property = "studentsName", column = "students_name"),
            @Result(property = "transferredAmount", column = "transferred_amount"),
            @Result(property = "yearOfPayment", column = "year_of_payment"),
    })
    @Select("SELECT students_name, transferred_amount, year_of_payment " +
            "FROM students " +
            "INNER JOIN annual_payment ON students.students_id = annual_payment.students_id " +
            "WHERE students.students_id=#{id} " +
            "ORDER BY annual_payment.year_of_payment")
    List<StudentsPaymentByID> getStudentsPaymentByID(Integer id);

}
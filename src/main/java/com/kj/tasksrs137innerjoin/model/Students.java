package com.kj.tasksrs137innerjoin.model;

public class Students {
    private Integer studentsID;
    private String studentsName;
    private String studentsContact;

    public Integer getStudentsID() {
        return studentsID;
    }

    public void setStudentsID(Integer studentsID) {
        this.studentsID = studentsID;
    }

    public String getStudentsName() {
        return studentsName;
    }

    public void setStudentsName(String studentsName) {
        this.studentsName = studentsName;
    }

    public String getStudentsContact() {
        return studentsContact;
    }

    public void setStudentsContact(String studentsContact) {
        this.studentsContact = studentsContact;
    }

    @Override
    public String toString() {
        return "students{" +
                "studentsID=" + studentsID +
                ", studentsName='" + studentsName + '\'' +
                ", studentsContact='" + studentsContact + '\'' +
                '}';
    }
}

package com.kj.tasksrs137innerjoin.model;

import java.util.Date;

public class AnnualPayment {
    private Integer paymentID;
    private Integer studentsID;
    private Integer transferredAmount;
    private Date yearOfPayment;

    public Integer getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(Integer paymentID) {
        this.paymentID = paymentID;
    }

    public Integer getStudentsID() {
        return studentsID;
    }

    public void setStudentsID(Integer studentsID) {
        this.studentsID = studentsID;
    }

    public Integer getTransferredAmount() {
        return transferredAmount;
    }

    public void setTransferredAmount(Integer transferredAmount) {
        this.transferredAmount = transferredAmount;
    }

    public Date getYearOfPayment() {
        return yearOfPayment;
    }

    public void setYearOfPayment(Date yearOfPayment) {
        this.yearOfPayment = yearOfPayment;
    }

    @Override
    public String toString() {
        return "annual_payment{" +
                "paymentID=" + paymentID +
                ", studentsID=" + studentsID +
                ", transferredAmount=" + transferredAmount +
                ", yearOfPayment='" + yearOfPayment + '\'' +
                '}';
    }
}

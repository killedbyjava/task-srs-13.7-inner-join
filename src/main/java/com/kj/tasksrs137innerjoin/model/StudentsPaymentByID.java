package com.kj.tasksrs137innerjoin.model;

public class  StudentsPaymentByID{
    private String studentsName;
    private Integer transferredAmount;
    private String yearOfPayment;

    public String getStudentsName() {
        return studentsName;
    }

    public void setStudentsName(String studentsName) {
        this.studentsName = studentsName;
    }

    public Integer getTransferredAmount() {
        return transferredAmount;
    }

    public void setTransferredAmount(Integer transferredAmount) {
        this.transferredAmount = transferredAmount;
    }

    public String getYear_of_payment() {
        return yearOfPayment;
    }

    public void setYearOfPayment(String yearOfPayment) {
        this.yearOfPayment = yearOfPayment;
    }

    @Override
    public String toString() {
        return "StudentsPaymentByID{" +
                "studentsName='" + studentsName + '\'' +
                ", transferredAmount=" + transferredAmount +
                ", yearOfPayment='" + yearOfPayment + '\'' +
                '}';
    }
}

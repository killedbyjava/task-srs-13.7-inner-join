CREATE DATABASE "university"
    WITH
    OWNER = rustam
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

    CREATE TABLE public.students
(
    students_id bigserial NOT NULL,
    students_name character varying(100) NOT NULL,
    students_contact character varying(20),
    PRIMARY KEY (students_id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.students
    OWNER to rustam;

    CREATE TABLE public.annual_payment
(
    payment_id bigserial NOT NULL,
    students_id integer NOT NULL,
    transferred_amount integer NOT NULL,
    year_of_payment date NOT NULL,
    PRIMARY KEY (payment_id),
    FOREIGN KEY (students_id)
        REFERENCES public.students (students_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.annual_payment
    OWNER to rustam;

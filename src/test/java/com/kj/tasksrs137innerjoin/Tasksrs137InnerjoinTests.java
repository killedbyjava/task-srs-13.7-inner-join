package com.kj.tasksrs137innerjoin;

import com.kj.tasksrs137innerjoin.dao.mapper.StudentsMapper;
import com.kj.tasksrs137innerjoin.model.AnnualPayment;
import com.kj.tasksrs137innerjoin.model.Students;
import com.kj.tasksrs137innerjoin.model.StudentsPaymentByID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.sql.Time;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import static org.postgresql.jdbc.EscapedFunctions.NOW;


@RunWith(SpringRunner.class)
@SpringBootTest
public class Tasksrs137InnerjoinTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(Tasksrs137InnerjoinTests.class);

	@Inject
	StudentsMapper studentsMapper;

/*   @Test
    public void testBasicCRUDOperation() {
        Students students = studentsMapper.getStudentsByID(3);
        LOGGER.info("----------------------------------> students: {}", students.getStudentsName());
        }
*/

	       //SELECT
/*            @Test
            public void testBasicCRUDOperation () {
                List<Students> list = studentsMapper.getAllData();
                LOGGER.info("----------------------------------> students: {}", list);
                }
*/


	//INSERT

	@Test
	public void TestInsertDataIntoStudents() {
		Students students = new Students();
		students.setStudentsName("Ivan Ivanov");
		students.setStudentsContact("79112223344");
		studentsMapper.insertDataIntoStudents(students);
		LOGGER.info("----------------------------------> students id: {}", students.getStudentsID());
	}

	@Test
	public void testInsertDataIntoStudents2() {
		Students students = new Students();
		students.setStudentsName("Petr Petrov");
		students.setStudentsContact("79112222222");
		studentsMapper.insertDataIntoStudents(students);
		LOGGER.info("----------------------------------> students id: {}", students.getStudentsID());
	}

	@Test
	public void testInsertDataIntoStudents3() {
		Students students = new Students();
		students.setStudentsName("Sidor Sidorov");
		students.setStudentsContact("79113333333");
		studentsMapper.insertDataIntoStudents(students);
		LOGGER.info("----------------------------------> students id: {}", students.getStudentsID());
	}

	@Test
	public void testInsertDataIntoAnnualPayment() {
		AnnualPayment annualPayment = new AnnualPayment();
		//annualPayment.setPaymentID();
		annualPayment.setStudentsID(3);
		annualPayment.setTransferredAmount(10000);
		annualPayment.setYearOfPayment(new Date());
		studentsMapper.insertDataIntoAnnualPayment(annualPayment);
		LOGGER.info("----------------------------------> students id: {}", annualPayment.getStudentsID());
	}

/*	@Test
	public void testGetAnnualPaymentByID () {
		List<AnnualPayment> list = studentsMapper.getAnnualPaymentByID(2);
		LOGGER.info("----------------------------------> annualPayment: {}", list);
	}*/

	@Test
	public void testGetStudentsPaymentsByID () {
		List<StudentsPaymentByID> list = studentsMapper.getStudentsPaymentByID(3);
		LOGGER.info("----------------------------------> students payment by ID: {}", list);
	}

}

